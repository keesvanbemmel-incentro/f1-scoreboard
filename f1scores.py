import socket
import struct
import time
import thread
from threading import Timer
import firebase_admin
from firebase_admin import credentials
from firebase_admin import db
from firebase_admin import storage
from cv2 import *
import os


UDP_IP = "127.0.0.1"
UDP_PORT = 20777
PHOTOINTERVAL = 10
NEWSESSIONTIMEOUT = 20

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

sock.bind(("", UDP_PORT))

current_milli_time = lambda: int(round(time.time() * 1000))

# global variables
oldphototimer = current_milli_time()
activesessionid = -1
nospeedtimer = None
sessionfastestlaptime = 99999
lastrecordedlaptime = 99999
photothread = None
latestSessionPicture = None

#Firebase stuff
cred = credentials.Certificate("firebasekey.json")
firebase_admin.initialize_app(cred, {
    'databaseURL' : 'https://f1-scoreboard.firebaseio.com',
    'storageBucket': 'f1-scoreboard.appspot.com'
})

root = db.reference()
bucket = storage.bucket()

# Define a function for the thread
def print_time():
   global activesessionid
   global bucket
   global latestSessionPicture
   while True:
    if (activesessionid == -1):
        break
    newFileName = str(current_milli_time())+".png"
    path = os.path.dirname(os.path.realpath(__file__))
    filePath = os.path.join(path,  str(activesessionid))
    if not os.path.exists(filePath):
        os.makedirs(filePath)
    absoluteFilePosition = os.path.join(filePath , str(newFileName))
    
    # initialize the camera
    cam = VideoCapture(0) # 0 -> index of camera
    time.sleep(3) # sleep a while for camera to get ready, otherwise pictures are dark
    s, img = cam.read()
    if s:    # frame captured without any errors
        imwrite(absoluteFilePosition,img) #save image

    cam.release()
    # check again for inactive session to avoid -1 pictures being taken in 3 second delay above
    if (activesessionid == -1):
        break
    # Upload a local file to a new file to be created in your bucket.
    firebaseStorageLocation = 'sessions/'+str(activesessionid)+'/'+str(newFileName)
    aBlob = bucket.blob(firebaseStorageLocation)
    aBlob.upload_from_filename(filename=absoluteFilePosition)
    latestSessionPicture = firebaseStorageLocation
    time.sleep(PHOTOINTERVAL)

def takeanduploadpicture():
    global bucket
    global activesessionid
    if (activesessionid == -1):
        return
    newFileName = str(current_milli_time())+".png"
    path = os.path.dirname(os.path.realpath(__file__))
    filePath = os.path.join(path,  str(activesessionid))
    if not os.path.exists(filePath):
        os.makedirs(filePath)
    absoluteFilePosition = os.path.join(filePath , str(newFileName))
    
    # initialize the camera
    cam = VideoCapture(0) # 0 -> index of camera
    time.sleep(3) # sleep a while for camera to get ready, otherwise pictures are dark
    s, img = cam.read()
    if s:    # frame captured without any errors
        imwrite(absoluteFilePosition,img) #save image

    cam.release()
    # Upload a local file to a new file to be created in your bucket.
    aBlob = bucket.blob('sessions/'+str(activesessionid)+'/'+str(newFileName))
    aBlob.upload_from_filename(filename=absoluteFilePosition)
    return 'sessions/'+str(activesessionid)+'/'+str(newFileName)

def sessiontimeout():
    global activesessionid
    global sessionfastestlaptime
    global root
    global photothread
    global latestSessionPicture
    if photothread is not None:
        photothread = None

    print("KILLING ACTIVE SESSION WITH ID", activesessionid)
    if (activesessionid > -1):
        unclaimedsession = root.child('unclaimed').child(str(activesessionid)).update({
            'finished': True
        })
    activesessionid = -1
    sessionfastestlaptime = 99999
    latestSessionPicture = None
    activesession = root.child('activesession').set({
                    'sessionid' : -1
    })

def runner():
    global activesessionid
    global nospeedtimer
    global oldphototimer
    global sessionfastestlaptime
    global lastrecordedlaptime
    global photothread
    global root
    global latestSessionPicture
    while True:
        t = Timer(NEWSESSIONTIMEOUT, sessiontimeout)
        t.start()
        data, addr = sock.recvfrom(1024)
        t.cancel()
        speedkmh = (struct.unpack('f', data[28:32])[0]*3.6)   # speed in meters/sec
        if speedkmh > 10.0:
            # we are driving!
            if nospeedtimer is not None:
                nospeedtimer.cancel()
                nospeedtimer = None

            lastlaptime = struct.unpack('f', data[248:252])[0] # last laptime
            laptime = struct.unpack('f', data[4:8])[0]
            drivingdistance = struct.unpack('f', data[8:12])[0]    # distance in meters
            outputtracknumber = struct.unpack('f', data[272:276])[0] # track number
            lastlaptime = struct.unpack('f', data[248:252])[0] # last laptime
            playercarindex = ord(struct.unpack('c', data[336:337])[0])
            teamid = ord(struct.unpack('c', data[(374+(45*playercarindex)):375+(45*playercarindex)])[0])
            sector = ord(struct.unpack('c', data[(379+(45*playercarindex)):380+(45*playercarindex)])[0])

            if activesessionid < 0:
                activesessionid  = current_milli_time()
                # get data from UDP
                # output = struct.unpack('f', data[132:136])  # gear
                # data is 1024 length bytearray, break into 4 bytes and get the output
                #currentlapinvalid = struct.unpack('c', data[315:316])[0]

                # create session in firebase
                activesession = root.child('activesession').set({
                    'sessionid' : activesessionid
                })

                unclaimedsession = root.child('unclaimed').child(str(activesessionid)).set({
                    'fastestLap': sessionfastestlaptime,
                    'trackNumber': int(outputtracknumber)
                })

                if photothread is None:
                    photothread = thread.start_new_thread( print_time, ( ) )
                print ("CREATED NEW SESSION WITH ID", activesessionid)

            if ((lastrecordedlaptime != lastlaptime) and (lastlaptime > 0)):
                # NEW LAP TIME! Add to session!
                #print ("New laptime: ", lastlaptime)
                lastrecordedlaptime = lastlaptime
                if (lastlaptime < sessionfastestlaptime):
                    # when someone steps in, lastlaptime still reflects old driver's time
                    # TODO build logic to overcome this
                    sessionfastestlaptime = lastlaptime
                    #print ("TAKING FINISH PICTURE FOR SESSION", activesessionid)
                    #lapFinishPicture = takeanduploadpicture()
                    print ("SESSION FASTEST: ", lastlaptime, " FOR SESSION", activesessionid)

                    unclaimedsession = root.child('unclaimed').child(str(activesessionid)).set({
                        'fastestLap': sessionfastestlaptime,
                        'trackNumber': int(outputtracknumber),
                        'sessionPicture': latestSessionPicture
                    })

            # # take a picture for ML training purposes
            # if ((current_milli_time() - oldphototimer)/1000) > PHOTOINTERVAL:
            #     oldphototimer = current_milli_time()
            #     takeanduploadpicture()
            #     print ("TAKING TRAINING SET PICTURE FOR SESSION", activesessionid)


            
            #print ("KM/H: ",speedkmh," Track:", outputtracknumber[0], " Last Lap time: ", outputlaptime[0])
        else:
            if nospeedtimer is None:
                nospeedtimer = Timer(NEWSESSIONTIMEOUT, sessiontimeout)
                nospeedtimer.start()
            # kill active session if longer than 10 seconds
runner()